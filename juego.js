document.getElementById("jugar").addEventListener("click", function( event ) {
  event.preventDefault();
  let jugadas = ["Piedra", "Papel", "Tijera"]
  var select = document.getElementById('juego');
  var player1 = select.options[select.selectedIndex].value;
  var player2 = getRandomArbitrary(0,3);
  if(player2 == player1){
    alert("Empate, elige otro");
    return false;
  }
  var ganador = calcularGanador(player1, player2);
  console.log(ganador);
  var plantilla = `
  <div class="alert alert-${ganador? "success":"danger"}" role="alert">
    <h4 class="alert-heading">¡Has ${ganador? "Ganado":"Perdido"}!</h4>
    <p>
      Elegiste: <span class="badge bg-primary">${jugadas[player1]}</span>
      El ordenador eligió: <span class="badge bg-secondary">${jugadas[player2]}</span>
    </p>
  </div>
`;
var body = document.getElementById("resultado");
body.innerHTML = plantilla;
}, false);


// Retorna un número aleatorio entre min (incluido) y max (excluido)
function getRandomArbitrary(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function calcularGanador(player1, player2) {
  if(player1 == 0) {
    var ganador = player2 == 1 ? false : true;
  }
  if(player1 == 1) {
    var ganador = player2== 2 ? false : true;
  }
  if(player1 == 2) {
    var ganador = player2 == 0 ? false : true;
  }
  return ganador;
}
